# Translation of ksquares to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2008, 2010, 2016, 2022.
msgid ""
msgstr ""
"Project-Id-Version: ksquares\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-26 00:42+0000\n"
"PO-Revision-Date: 2022-06-19 14:01+0200\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.04.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Karl Ove Hufthammer"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "karl@huftis.org"

#. i18n: ectx: label, entry (NumOfPlayers), group (Game Settings)
#: ksquares.kcfg:10
#, kde-format
msgid "Number of Players"
msgstr "Talet på spelarar"

#. i18n: ectx: label, entry (PlayerNames), group (Game Settings)
#: ksquares.kcfg:14
#, kde-format
msgid "Player Names"
msgstr "Spelarnamn"

#. i18n: ectx: label, entry (HumanList), group (Game Settings)
#: ksquares.kcfg:17
#, kde-format
msgid "Human or AI"
msgstr "Menneske eller datamaskin"

#. i18n: ectx: label, entry (BoardWidth), group (Game Settings)
#: ksquares.kcfg:21
#, kde-format
msgid "Width of board"
msgstr "Breidda på spelebrettet"

#. i18n: ectx: label, entry (BoardHeight), group (Game Settings)
#: ksquares.kcfg:25
#, kde-format
msgid "Height of board"
msgstr "Høgda på spelebrettet"

#. i18n: ectx: label, entry (QuickStart), group (Game Settings)
#: ksquares.kcfg:29
#, kde-format
msgid "Quick start the game"
msgstr "Snøggstart av spelet"

#. i18n: ectx: label, entry (Difficulty), group (AI)
#: ksquares.kcfg:35
#, kde-format
msgid "Difficulty"
msgstr "Vanskegrad"

#. i18n: ectx: whatsthis, entry (Difficulty), group (AI)
#: ksquares.kcfg:36
#, kde-format
msgid "How difficult the AI should be"
msgstr "Kor kraftig datamotstandaren skal vera"

#. i18n: ectx: label, entry (LineColor), group (Display)
#: ksquares.kcfg:42
#, kde-format
msgid "Line Color"
msgstr "Linjefarge"

#. i18n: ectx: label, entry (IndicatorLineColor), group (Display)
#: ksquares.kcfg:47
#, kde-format
msgid "Indicator Line Color"
msgstr "Indikatorlinjefarge"

#. i18n: ectx: label, entry (HighlightColor), group (Display)
#: ksquares.kcfg:52
#, kde-format
msgid "Highlight Color"
msgstr "Markeringsfarge"

#: ksquaresdemowindow.cpp:65
#, kde-format
msgid "Player %1"
msgstr "Spelar %1"

#. i18n: ectx: ToolBar (mainToolBar)
#: ksquaresui.rc:11
#, kde-format
msgid "Main Toolbar"
msgstr "Hovudverktøylinje"

#: ksquareswindow.cpp:57
#, kde-format
msgid "Current Player"
msgstr "Gjeldande spelar"

#. i18n: ectx: property (text), item, widget (KComboBox, kcfg_Difficulty)
#: ksquareswindow.cpp:68 ksquareswindow.cpp:229 ksquareswindow.cpp:235
#: ksquareswindow.cpp:240 prefs_ai.ui:54
#, kde-format
msgid "Easy"
msgstr "Lett"

#. i18n: ectx: property (text), item, widget (KComboBox, kcfg_Difficulty)
#: ksquareswindow.cpp:69 ksquareswindow.cpp:230 ksquareswindow.cpp:234
#: ksquareswindow.cpp:241 prefs_ai.ui:59
#, kde-format
msgid "Medium"
msgstr "Middels"

#. i18n: ectx: property (text), item, widget (KComboBox, kcfg_Difficulty)
#: ksquareswindow.cpp:70 ksquareswindow.cpp:231 ksquareswindow.cpp:236
#: ksquareswindow.cpp:239 prefs_ai.ui:64
#, kde-format
msgid "Hard"
msgstr "Vanskeleg"

#: ksquareswindow.cpp:201
#, kde-format
msgctxt "@title:window"
msgid "Scores"
msgstr "Poeng"

#: ksquareswindow.cpp:206
#, kde-format
msgid "Player Name"
msgstr "Spelarnamn"

#: ksquareswindow.cpp:207
#, kde-format
msgid "Completed Squares"
msgstr "Fullførte kvadrat"

#: ksquareswindow.cpp:291
#, kde-format
msgid "Start a new game with the current settings"
msgstr "Start eit nytt spel med gjeldande innstillingar"

#: ksquareswindow.cpp:309
#, kde-format
msgid "Display"
msgstr "Vising"

#: ksquareswindow.cpp:314
#, kde-format
msgid "Computer Player"
msgstr "Dataspelar"

#: main.cpp:39
#, kde-format
msgid "KSquares"
msgstr "Kvadrat"

#: main.cpp:41
#, kde-format
msgid ""
"Take it in turns to draw lines.\n"
"If you complete a squares, you get another go."
msgstr ""
"Byt på å teikna linjer.\n"
"Viss du fullfører eit kvadrat, er det din tur att."

#: main.cpp:43
#, kde-format
msgid "(C) 2006-2007 Matt Williams"
msgstr "© 2006–2007 Matt Williams"

#: main.cpp:46
#, kde-format
msgid "Matt Williams"
msgstr "Matt Williams"

#: main.cpp:46
#, kde-format
msgid "Original creator and maintainer"
msgstr "Opphavsperson og noverande vedlikehaldar"

#: main.cpp:47
#, kde-format
msgid "Fela Winkelmolen"
msgstr "Fela Winkelmolen"

#: main.cpp:47
#, kde-format
msgid "Many patches and bugfixes"
msgstr "Mange feilrettingar"

#: main.cpp:48
#, kde-format
msgid "Tom Vincent Peters"
msgstr "Tom Vincent Peters"

#: main.cpp:48
#, kde-format
msgid "Hard AI"
msgstr "Vanskeleg datamotstandar"

#: main.cpp:53
#, kde-format
msgid "Run game in demo (autoplay) mode"
msgstr "Køyr spelet i demomodus (automatisk speling)"

#: main.cpp:67 main.cpp:68 main.cpp:69
#, kde-format
msgctxt "default name of player"
msgid "Player %1"
msgstr "Spelar %1"

#: newgamedialog.cpp:28
#, kde-format
msgctxt "@title:window"
msgid "New Game"
msgstr "Nytt spel"

#. i18n: ectx: property (windowTitle), widget (QWidget, NewGameWidget)
#: newgamewidget.ui:13
#, kde-format
msgid "New Game"
msgstr "Nytt spel"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, groupBox)
#: newgamewidget.ui:37
#, kde-format
msgid "Settings for the players in the game"
msgstr "Innstillingar for spelarar"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: newgamewidget.ui:40
#, kde-format
msgid "Players"
msgstr "Spelarar"

#. i18n: ectx: property (whatsThis), widget (QLabel, labelPlayer4Name)
#: newgamewidget.ui:64
#, kde-format
msgid "The name of the fourth player"
msgstr "Namnet på den fjerde spelaren"

#. i18n: ectx: property (text), widget (QLabel, labelPlayer4Name)
#: newgamewidget.ui:67
#, kde-format
msgid "Player 4:"
msgstr "Spelar 4:"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, playerFourHuman)
#. i18n: ectx: property (whatsThis), widget (QCheckBox, playerThreeHuman)
#. i18n: ectx: property (whatsThis), widget (QCheckBox, playerOneHuman)
#. i18n: ectx: property (whatsThis), widget (QCheckBox, playerTwoHuman)
#: newgamewidget.ui:80 newgamewidget.ui:96 newgamewidget.ui:166
#: newgamewidget.ui:214
#, kde-format
msgid "Should this player be controlled by a human"
msgstr "Om denne spelaren skal styrast av eit menneske"

#. i18n: ectx: property (text), widget (QCheckBox, playerFourHuman)
#: newgamewidget.ui:83
#, kde-format
msgid "Hum&an?"
msgstr "Menne&ske?"

#. i18n: ectx: property (text), widget (QCheckBox, playerThreeHuman)
#: newgamewidget.ui:99
#, kde-format
msgid "Hu&man?"
msgstr "Me&nneske?"

#. i18n: ectx: property (whatsThis), widget (QLabel, labelPlayer3Name)
#: newgamewidget.ui:109
#, kde-format
msgid "The name of the third player"
msgstr "Namnet på den tredje spelaren"

#. i18n: ectx: property (text), widget (QLabel, labelPlayer3Name)
#: newgamewidget.ui:112
#, kde-format
msgid "Player 3:"
msgstr "Spelar 3:"

#. i18n: ectx: property (whatsThis), widget (QLabel, labelPlayer2Name)
#: newgamewidget.ui:131
#, kde-format
msgid "The name of the second player"
msgstr "Namnet på den andre spelaren?"

#. i18n: ectx: property (text), widget (QLabel, labelPlayer2Name)
#: newgamewidget.ui:134
#, kde-format
msgid "Player 2:"
msgstr "Spelar 2:"

#. i18n: ectx: property (text), widget (QCheckBox, playerOneHuman)
#: newgamewidget.ui:169
#, kde-format
msgid "&Human?"
msgstr "&Menneske?"

#. i18n: ectx: property (whatsThis), widget (QLabel, numOfPlayersLabel)
#: newgamewidget.ui:182
#, kde-format
msgid ""
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/"
"css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-"
"weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\">How many players will be "
"in the game</p></body></html>"
msgstr ""
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/"
"css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-"
"weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\">Kor mange spelarar skal "
"vera med?</p></body></html>"

#. i18n: ectx: property (text), widget (QLabel, numOfPlayersLabel)
#: newgamewidget.ui:185
#, kde-format
msgid "Number of players:"
msgstr "Talet på spelarar:"

#. i18n: ectx: property (whatsThis), widget (QLabel, labelPlayer1Name)
#: newgamewidget.ui:198
#, kde-format
msgid "The name of the first player"
msgstr "Namnet på den første spelaren"

#. i18n: ectx: property (text), widget (QLabel, labelPlayer1Name)
#: newgamewidget.ui:201
#, kde-format
msgid "Player 1:"
msgstr "Spelar 1:"

#. i18n: ectx: property (text), widget (QCheckBox, playerTwoHuman)
#: newgamewidget.ui:217
#, kde-format
msgid "H&uman?"
msgstr "M&enneske?"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, groupBox_2)
#: newgamewidget.ui:230
#, kde-format
msgid "Settings for the game board"
msgstr "Innstillingar for spelebrettet"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#: newgamewidget.ui:233
#, kde-format
msgid "Game Board"
msgstr "Spelebrett"

#. i18n: ectx: property (whatsThis), widget (QLabel, labelWidth)
#: newgamewidget.ui:257
#, kde-format
msgid "The number of squares the area is wide"
msgstr "Breidda på speleområdet, målt i talet på kvadrat"

#. i18n: ectx: property (text), widget (QLabel, labelWidth)
#: newgamewidget.ui:260
#, kde-format
msgid "Width:"
msgstr "Breidd:"

#. i18n: ectx: property (whatsThis), widget (QLabel, labelHeight)
#: newgamewidget.ui:289
#, kde-format
msgid "The number of squares the area is high"
msgstr "Høgda på speleområdet, målt i talet på kvadrat"

#. i18n: ectx: property (text), widget (QLabel, labelHeight)
#: newgamewidget.ui:292
#, kde-format
msgid "Height:"
msgstr "Høgd:"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, groupBox_3)
#: newgamewidget.ui:324
#, kde-format
msgid "Settings for the game"
msgstr "Innstillingar for spelet"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_3)
#: newgamewidget.ui:327
#, kde-format
msgid "Game Settings"
msgstr "Spelinnstillingar"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, quickStartCheck)
#: newgamewidget.ui:351
#, kde-format
msgid "Partially fill in the board automatically before the game starts"
msgstr "Fyll inn spelebrett delvis før spelet startar"

#. i18n: ectx: property (text), widget (QCheckBox, quickStartCheck)
#: newgamewidget.ui:354
#, kde-format
msgid "Quick start"
msgstr "Snøggstart"

#. i18n: ectx: property (windowTitle), widget (QWidget, prefs_ai)
#: prefs_ai.ui:13
#, kde-format
msgid "AI Settings"
msgstr "Datamotstandar"

#. i18n: ectx: property (whatsThis), widget (QLabel, label_difficulty)
#: prefs_ai.ui:75
#, kde-format
msgid "How difficult it will be to beat the computer"
msgstr "Kor vanskeleg det skal vera å slå datamaskina"

#. i18n: ectx: property (text), widget (QLabel, label_difficulty)
#: prefs_ai.ui:78
#, kde-format
msgid "AI difficulty:"
msgstr "Vanskegrad:"

#. i18n: ectx: property (windowTitle), widget (QWidget, prefs_display)
#: prefs_display.ui:13
#, kde-format
msgid "Display Settings"
msgstr "Innstillingar for vising"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, groupBox_2)
#: prefs_display.ui:19
#, kde-format
msgid "Settings for colors"
msgstr "Innstillingar for farge"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#: prefs_display.ui:22
#, kde-format
msgid "Colors"
msgstr "Fargar"

#. i18n: ectx: property (whatsThis), widget (QLabel, label)
#: prefs_display.ui:28
#, kde-format
msgid "Color of the drawn lines"
msgstr "Farge på dei teikna linjene"

#. i18n: ectx: property (text), widget (QLabel, label)
#: prefs_display.ui:31
#, kde-format
msgid "Standard line color:"
msgstr "Standard linjefarge:"

#. i18n: ectx: property (whatsThis), widget (QLabel, label_2)
#. i18n: ectx: property (whatsThis), widget (QLabel, label_3)
#: prefs_display.ui:44 prefs_display.ui:60
#, kde-format
msgid "Color of the indicator lines"
msgstr "Fargen på indikatorlinjene"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: prefs_display.ui:47
#, kde-format
msgid "Indicator line color:"
msgstr "Farge på indikatorlinje:"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: prefs_display.ui:63
#, kde-format
msgid "Highlight color:"
msgstr "Markeringsfarge:"

#. i18n: ectx: property (windowTitle), widget (QWidget, ScoresWidget)
#: scoreswidget.ui:13
#, kde-format
msgid "Scores"
msgstr "Poeng"

#. i18n: ectx: property (whatsThis), widget (QTableView, scoreTable)
#: scoreswidget.ui:19
#, kde-format
msgid "Scoreboard"
msgstr "Rekordar"
